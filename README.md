## Setup
All the setup and config values are in config.json

Frontend-url is url where bot is hosted
Backend-url is url where api is hosted
page_access_token is Facebook page access token and can be retrieved from the facebook app page

## Deployment

The bot is a simple node.js application and follows standard node.js application hosting

[Heorku](https://devcenter.heroku.com/articles/deploying-nodejs)
[Azure](https://docs.microsoft.com/en-us/azure/cloud-services/cloud-services-nodejs-develop-deploy-app)
[Aws](https://aws.amazon.com/getting-started/projects/deploy-nodejs-web-app/)

## Test Accounts

username: akshay
company: ozz
password: password

username: kwakc
company: justlogin
password: password