var builder = require('botbuilder');
var restify = require('restify');
var request = require('request');
var apiai   = require('apiai');
var sleep   = require('sleep');
var apiairecognizer = require('api-ai-recognizer');

var app = apiai("36fb90fe7b7b4842a6bce96edaf46c86");

var fs = require('fs');
var config = JSON.parse(fs.readFileSync('config.json', 'utf8'));


var server = restify.createServer();
server.listen(process.env.PORT || process.env.port || 3978, function(){
    console.log('%s is listening to %s',server.name, server.url);
});


//=========Static File Serving and API Mock=======================================================================//
server.get(/\/static\/?.*/, restify.serveStatic({
  directory: __dirname
}));

server.get("/getDates", restify.queryParser(), function(req,res,next){
	if (req.query && req.query.fromDate && req.query.toDate && req.query.fromTime && req.query.toTime && req.query.psid){
		var psid     = req.query.psid;

		var fromDate = req.query.fromDate;
		var toDate   = req.query.toDate;
		var fromTime = req.query.fromTime;
		var toTime   = req.query.toTime;

		var timeStr = "from: " + fromDate + " " + fromTime + " to: " + toDate + " " + toTime;
		sendGenericMessage(psid, timeStr);
		res.send("Done");
	}else{
		res.send("Invalid");
	}
});

server.get('/authorize',restify.queryParser(),function(req,res,next){
	if (req.query && req.query.redirect_uri && req.query.username) {
    		var username = req.query.username;
			var company  = req.query.company;
			var password = req.query.password;

			var options = { method: 'POST',
			url: config["backend_url"] + '/authenticate',
			headers: 
			{ 'postman-token': '427625e2-dbd6-b74f-c914-dd6148986be9',
				'cache-control': 'no-cache',
				'content-type': 'application/json' },
			body: { Company_Id: company, Username: username, Password: password },
			json: true };

			request(options, function (error, response, body) {
				if (error) throw new Error(error);

				if (body.user_id != 0)
				{
					var auth_code = username + "**" + company + "**" + body.user_id; 
			
					var redirectUri = req.query.redirect_uri + '&authorization_code=' + auth_code;
					return res.redirect(redirectUri, next);
				} else{
					res.send("Invalid credentials");
				}
				
			});
	} else{
		return res.send(400, 'Request did not contain redirect_uri and username in the query string');
	}
});

// API Mocks

server.post('/authenticate',restify.bodyParser(), function(req,res){
	 data = req.body;

	 username = req.body.username;
	 companyId = req.body.companyId;
	 password  = req.body.password;

	 if (username == "akshay" && companyId=="ozz" && password=="password"){
		 var result = {success:true, userId:1};
	 } else if (username == "kwakc" && companyId=="justlogin" && password=="password"){
		 var result = {success:true,userId:3};
	 } else{
		 var result = {success:false};
	 }
	 res.send(result);
});
//====================================================================================================//

var connector = new builder.ChatConnector({
    appId: config["microsoft_app_id"],
    appPassword: config["microsoft_app_secret"]
});


var bot = new builder.UniversalBot(connector);
server.post('/api/messages',connector.listen());

var recognizer = new apiairecognizer('70cbdadb7d184803a88c6f8f56ea5196');
var intents = new builder.IntentDialog({
	recognizers:[recognizer]
});

bot.dialog('/', intents);

intents.matches('reset', function(session, args){
	session.send("done");
	session.userData = {};
});

intents.matches('smalltalk.greetings.hello',function(session,args){
	session.send("Hey " + session.message.user.name);
	var replyMessage = new builder.Message(session)
                                            .text("I can help you apply for leave");

	replyMessage.sourceEvent({ 
				facebook: { 
					quick_replies: [{
						content_type:"text",
						title:"Apply for leave",
						payload:"apply for leave"
					},
					{
						content_type:"text",
						title:"Check leave balance",
						payload:"check leave balance"
					}]
				}
			});
    session.send(replyMessage);
});

intents.matches('smalltalk.appraisal.thank_you', function(session,args){
	var fulfillment =  builder.EntityRecognizer.findEntity(args.entities, 'fulfillment');
	if (fulfillment){
			var speech = fulfillment.entity;
			session.send(speech);
	}else{
		session.send('Sorry...not sure how to respond to that');
	}
});

intents.matches('smalltalk.greetings.goodmorning', function(session,args){
	var fulfillment =  builder.EntityRecognizer.findEntity(args.entities, 'fulfillment');
	if (fulfillment){
			var speech = fulfillment.entity;
			session.send(speech);
	}else{
		session.send('Sorry...not sure how to respond to that');
	}
});

intents.matches('login',function(session){
	session.beginDialog("/login")
});

intents.matches('type', function(session,args){
	if (session.userData.companyId && session.userData.companyId != 'undefined'){
		var type = builder.EntityRecognizer.findEntity(args.entities,"leaveTypes");
		if (type){
			type = type.entity;
			session.userData.leaveType1 = type;
		}
		session.beginDialog("/setType");
	}else{
		session.send("You need to log into your justlogin account before proceeding");
		session.userData.lastAction = "applyLeave";
		session.beginDialog("/login");
	}
});

intents.matches('dates',function(session){
	if (session.userData.companyId && session.userData.companyId != 'undefined'){
		session.beginDialog("/setDates");
	}else{
		session.send("You need to log into your justlogin account before proceeding");
		session.userData.lastAction = "applyLeave";
		session.beginDialog("/login");
	}
});

intents.matches('todayLeave', function(session, args){
	session.userData.leaveQuestion = "apply";
	if (session.userData.companyId && session.userData.companyId != 'undefined'){
		var leaveType = builder.EntityRecognizer.findEntity(args.entities, 'leaveTypes');
		var date = builder.EntityRecognizer.findEntity(args.entities,'date');

		var fromDateParts = date.entity.split('-');

		var fromDate = fromDateParts[1] + "-" + fromDateParts[2] + "-" + fromDateParts[0];
		var toDate = fromDate;

		session.userData.fromDate = fromDate;//datePeriod[0];
		session.userData.toDate = toDate;//datePeriod

		if (leaveType){
			leaveType = leaveType.entity;

			if (session.userData.leaveTypes.indexOf(leaveType)<=-1){
				session.endDialog("Sorry you don't have that type of leave available");
			}
			session.userData.leaveType = leaveType;
		}
		session.beginDialog("/applyLeave");
	}else{
		session.send("You need to log into your justlogin account before proceeding");
		session.userData.lastAction = "applyLeave";
		session.beginDialog("/login");
	}
});

intents.matches('leaveApply',function(session,args){
	session.userData.leaveQuestion = "apply";
	if (session.userData.companyId && session.userData.companyId != 'undefined'){
		var leaveType = builder.EntityRecognizer.findEntity(args.entities, 'leaveTypes');
		var date = builder.EntityRecognizer.findEntity(args.entities,'date');
		var datePeriod = builder.EntityRecognizer.findEntity(args.entities, 'date-period');
		var indicator = builder.EntityRecognizer.findEntity(args.entities,'indicator');
		if(datePeriod){
			datePeriod = datePeriod.entity.split("/");
			if (datePeriod.length == 2)
			{
				var fromDateParts = datePeriod[0].split('-');
				var toDateParts   = datePeriod[1].split('-');

				var fromDate = fromDateParts[1] + "-" + fromDateParts[2] + "-" + fromDateParts[0];
				var toDate = toDateParts[1] + "-" + toDateParts[2] + "-" + toDateParts[0];
				session.userData.fromDate = fromDate;//datePeriod[0];
				session.userData.toDate = toDate;//datePeriod[1];
			}
		} else if(indicator){
			indicator = indicator.entity;
			session.userData.indicator = indicator
		}
		if (date){
			date = date.entity;

			if (date.constructor === Array){
				var fromDateParts = date[0].split('-');
				var toDateParts   = date[1].split('-');

				var fromDate = fromDateParts[1] + "-" + fromDateParts[2] + "-" + fromDateParts[0];
				var toDate = toDateParts[1] + "-" + toDateParts[2] + "-" + toDateParts[0];
				session.userData.fromDate = fromDate;//datePeriod[0];
				session.userData.toDate = toDate;//datePeriod[1];
			}else{
				session.userData.date = date;
			}
		}
		if (leaveType){
			leaveType = leaveType.entity;

			if (session.userData.leaveTypes.indexOf(leaveType)<=-1){
				session.endDialog("Sorry you don't have that type of leave available");
			}
			session.userData.leaveType = leaveType;
		}
		session.beginDialog("/applyLeave");
	}else{
		session.send("You need to log into your justlogin account before proceeding");
		session.userData.lastAction = "applyLeave";
		session.beginDialog("/login");
	}
	
});

intents.matches('checkBalance',function(session, args){
	session.userData.leaveQuestion = "balance";
	if (session.userData.companyId && session.userData.companyId != 'undefined'){
		var leaveType = builder.EntityRecognizer.findEntity(args.entities,"leaveTypes")
		if (leaveType){
			leaveType = leaveType.entity;
			session.userData.leaveType = leaveType;
		}
		session.beginDialog("/checkBalance");
	}else{
		session.send("You need to log into your justlogin account before proceeding");
		session.userData.lastAction = "checkBalance";
		session.beginDialog("/login");
	}
});


intents.matches('logout',
  function (session) {
    request({
      url: 'https://graph.facebook.com/v2.6/me/unlink_accounts',
      method: 'POST',
      qs: {
        access_token: config["page_access_token"]
      },
      body: {
        psid: session.message.address.user.id
      },
      json: true
    }, function (error, response, body) {
      if (!error && response.statusCode === 200) {
        // No need to do anything send anything to the user
        // in the success case since we respond only after
        // we have received the account unlinking webhook
        // event from Facebook.
        session.endDialog();
      } else {
        session.endDialog('Error while unlinking account');
      }
    });
  }
);

intents.matches('handleDates', function(session, args){
	if (session.userData.companyId && session.userData.companyId != 'undefined'){
		var dates = builder.EntityRecognizer.findEntity(args.entities, 'date-time').entity;

		var fromDate = dates[0]
		var toDate   = dates[1]

		var fromTime = builder.EntityRecognizer.findEntity(args.entities, 'time').entity;
		var toTime = builder.EntityRecognizer.findEntity(args.entities, 'time1').entity;

		session.userData.fromDate = fromDate;
		session.userData.toDate = toDate;
		session.userData.fromTime = fromTime;
		session.userData.toTime = toTime;

		session.userData.selectDates = true;

		session.beginDialog('/confirmDates');
	}else{
		session.send("You need to log into your justlogin account before proceeding");
		session.userData.lastAction = "applyLeave";
		session.beginDialog("/login");
	}
});

intents.onDefault(function(session){
	var accountLinking = session.message.sourceEvent.account_linking;
	if (accountLinking){
		var authorizationStatus = accountLinking.status;
		if (authorizationStatus === 'linked') {
			session.sendTyping();
			var data = accountLinking.authorization_code.split("**");
			var username = data[0];
			var company  = data[1];
			var userId   = data[2];
			// Persist username under the userData
			session.userData.username = username;
			session.userData.companyId = company;
			session.userData.userId = userId;
			//==========GetuserInfo=================//
				var options = { method: 'GET',
				url: config["backend_url"] + '/userguid',
				qs: { userid: username, companyid: company },
				headers: 
				{   'cache-control': 'no-cache',
					'content-type': 'application/json' },
				json: true };

				request(options, function (error, response, body) {
					if (error) throw new Error(error);
					session.userData.userGUID = body.userguid;
			//==========GetCompanyGUID==============//
			var options = { method: 'GET',
			url: config["backend_url"] + '/companyguid',
			qs: { userGUID: session.userData.userGUID },
			headers: 
			{ 	'cache-control': 'no-cache',
				'content-type': 'application/json' },
			json: true };

			request(options, function (error, response, body) {
				if (error) throw new Error(error);

				session.userData.companyGUID = body.companyGUID;

			// 	//===========GetLeaveTypes================//
				var options = { method: 'GET',
				url: config["backend_url"] + '/leavetypes',
				qs: { companyGUID: session.userData.companyGUID, userGUID:session.userData.userGUID, year: "2017" },
				headers: 
				{ 	'cache-control': 'no-cache',
					'content-type': 'application/json' },
				json: true };


				request(options, function (error, response, body) {
					if (error) throw new Error(error);

					var leaveTypes = [];
					var leaveIds = {};
					var leaveTypesTable = body.types[0].Table;

					for (var i=0;i<leaveTypesTable.length; i++){
						leaveTypes.push(leaveTypesTable[i]['leavename']);
						leaveIds[leaveTypesTable[i]['leavename']] = leaveTypesTable[i]['leaveType']
					}
					session.userData.leaveIds = leaveIds;
					session.userData.leaveTypes = leaveTypes;

					// 	//==========GetAORO Info=================//
					var options = { method: 'GET',
					url:config["backend_url"] +  '/useraoro',
					qs: { userGUID: session.userData.userGUID },
					headers: 
					{   'cache-control': 'no-cache',
						'content-type': 'application/json' },
					body: { Company_Id: 'testgvss', Username: 'admin', Password: 'pass' },
					json: true };

					request(options, function (error, response, body) {
					if (error) throw new Error(error);

						if (body.Aoname){
							session.userData.aoname = body.Aoname;
						}else{
							session.userData.aoname = "";
						}

						if (body.Roname){
							session.userData.roname = body.Roname;
						}else{
							session.userData.roname = "";
						}

						if (body.Aoguid){
							session.userData.aoguid = body.Aoguid;
						}else{
							session.userData.aoguid = "";
						}

						if (body.Roguid){
							session.userData.roguid = body.Roguid;
						}else{
							session.userData.roguid = "";
						}
						

						var lastAction = session.userData.lastAction

						if (lastAction == 'checkBalance'){
							session.beginDialog("/checkBalance");
						}else if (lastAction == 'applyLeave'){
							session.beginDialog("/applyLeave");
						}else{
							var replyMessage = new builder.Message(session)
										.text("I can help you apply for leave or check your leave balance");

							replyMessage.sourceEvent({ 
									facebook: { 
										quick_replies: [{
											content_type:"text",
											title:"Apply for leave",
											payload:"apply for leave"
										},
										{
											content_type:"text",
											title:"Check leave balance",
											payload:"check leave balance"
										}]
									}
								});
							session.endDialog(replyMessage);
						}
					});
				// 	//=======================================//
			});
			// 	//========================================//
				
			// 	//======================================//
			});
			//======================================//
		 });
		} else if (authorizationStatus === 'unlinked') {
			// Remove username from the userData
			session.userData = {};

			session.endDialog();
		} else{
			session.endDialog('Unknown account linking event received');
		}
	}else{
		session.send("Sorry...I couldn't understand");
		var replyMessage = new builder.Message(session)
                                            .text("I can help you apply for leave or check your leave balance");

		replyMessage.sourceEvent({ 
				facebook: { 
					quick_replies: [{
						content_type:"text",
						title:"Apply for leave",
						payload:"apply for leave"
					},
					{
						content_type:"text",
						title:"Check leave balance",
						payload:"check leave balance"
					}]
				}
			});
		session.send(replyMessage);
		}
});

bot.dialog('/login',function(session){
	var message = new builder.Message(session)
      .sourceEvent({
        facebook: {
          attachment: {
            type: 'template',
            payload: {
              template_type: 'generic',
              elements: [{
                title: 'Log into your justlogin account',
                image_url: 'https://res.cloudinary.com/crunchbase-production/image/upload/v1470360294/jvo3fcpicbmxkyjuvyio.png',
                buttons: [{
                  type: 'account_link',
                  url: config["frontend_url"] + '/static/index.html'
                }]
              }]
            }
          }
        }
      });
    session.endDialog(message);
});

bot.dialog('/setDates',[
function(session){
	if(session.userData.fromDate && session.userData.toDate){
		session.beginDialog("/getTimes");
	}
	// else if(session.userData.date){
	// 	if (session.userData.indicator == "to"){
	// 		session.beginDialog("/getFrom");
	// 	}else{
	// 		session.beginDialog("/getTo");
	// 	}
	// }
	else{
		if (!session.userData.leaveType){
			session.userData.leaveType = "";
		}
		var fb_id = session.message.user.id;
		var msg = new builder.Message(session).sourceEvent({
			//specify the channel

			facebook: {
			//format according to channel's requirements

			//(in our case, the above JSON required by Facebook)

			attachment: {
				type: "template",
				payload: {
				template_type: "generic",
				elements: [
				{
					title: "Select Dates",
					subtitle: 'Please select the from and to dates for ' + session.userData.leaveType + ' leave.',
					buttons: [
						{
						type: "web_url",
						url: config["frontend_url"] + "/static/dates.html?psid=" + session.message.user.id,
						title: "Select Dates"
					}
					]
				}
				]
				}
			} //end of attachment

			}
		});
		// var message = new builder.Message(session)
		// .sourceEvent({
		// 	facebook: {
		// 	attachment: {
		// 		type: 'template',
		// 		payload: {
		// 		template_type: 'button',
		// 		text:'Please select the from and to dates for ' + session.userData.leaveType + ' leave.',
		// 		buttons:[
		// 			{
		// 				"type":"web_url",
		// 				"url":config["frontend_url"] + "/static/dates.html?psid=" + session.message.user.id,
		// 				"title":"Select Dates",
		// 				"messenger_extensions": true
		// 			}
		// 		]
		// 		}
		// 	}
		// 	}
		// });
		session.endDialog(msg);
	}
}]);

bot.dialog('/setType', [
	function(session){
		if (!session.userData.leaveType1){
			var types = session.userData.leaveTypes.join('|');
			builder.Prompts.choice(session,"Choose leave type?",types);
		}else{
			session.userData.leaveType = session.userData.leaveType1;
			session.userData.leaveType1 = undefined;
			session.endDialog();
			if (session.userData.leaveQuestion){
				if (session.userData.leaveQuestion == "apply"){
					session.beginDialog("/applyLeave");
				}else{
					session.beginDialog("/checkBalance");
				}
			}else{
				session.beginDialog("/applyLeave");
			}
		}
},
	function(session,results){
		session.userData.leaveType = results.response.entity;
		if (session.userData.leaveQuestion){
			if (session.userData.leaveQuestion == "apply"){
				session.beginDialog("/applyLeave");
			}else{
				session.beginDialog("/checkBalance");
			}
		} else{
			session.beginDialog("/applyLeave");
		}
		
		session.endDialog();
	}
]);

bot.dialog("/applyLeave", [
	function(session){
		if (!session.userData.leaveType || session.userData.leaveType == "undefined"){
			var types = session.userData.leaveTypes.join('|');
			builder.Prompts.choice(session,"Choose leave type?",types);
		}else{
			session.endDialog();
			session.beginDialog("/setDates");
		}	
},
	function(session,results){
		if (!session.userData.leaveType || session.userData.leaveType == "undefined"){
			session.userData.leaveType = results.response.entity;
			session.endDialog();
			session.beginDialog("/setDates");
		} else{
			session.endDialog();
		}
	}
]);

bot.dialog("/checkBalance", [
	function(session){
		if (session.userData.leaveType){
			var options = { method: 'GET',
						url: config["backend_url"] + '/leavebalance',
						qs: { companyGUID: session.userData.companyGUID, userGUID:session.userData.userGUID, year: "2017" },
						headers: 
						{ 'cache-control': 'no-cache',
							'content-type': 'application/json' },
						json: true };

						request(options, function (error, response, body) {
							if (error) throw new Error(error);

							var leaveBalanceTable = body.leavebalance[0].Table;

							var leaveBalances = {};

							for (var i=0;i<leaveBalanceTable.length; i++){
								leaveBalances[leaveBalanceTable[i]['LeaveType']] = leaveBalanceTable[i]['Balance']
							}

							session.userData.leavebalance = leaveBalances;

							var leaveBalance = session.userData.leavebalance[session.userData.leaveType];
							session.endDialog("You have " + leaveBalance + " days of " + session.userData.leaveType + " leave left");	
							
						});
		}else{
			session.endDialog();
			session.beginDialog("/setType")
		}	
}]);

bot.dialog("/confirmDates",[
	function(session){
		if (new Date(session.userData.fromDate) > new Date(session.userData.toDate)){
			session.endDialog("From date has to be before to date");
			session.userData.fromDate = undefined;
			session.userData.toDate = undefined;
			session.beginDialog("/setDates");
		}else{
			session.send('Hold on for a few moments');
			session.endDialog();
			session.sendTyping();
			//getOverlapped(0,session);
			
			var options = {
				 method: 'GET',
				url: config["backend_url"] + '/days',
				qs: { 
					fromDate: session.userData.fromDate,
					toDate: session.userData.toDate,
					fromTime: session.userData.fromTime,
					toTime: session.userData.toTime,
					userGUID: session.userData.userGUID,
					companyGUID: session.userData.companyGUID 
				},
				headers: { 
					'cache-control': 'no-cache',
					'content-type': 'application/json' 
				},
				json: true 
			};
			//sleep.sleep(3);
			request(options, function (error, response, body) {
				if (error) throw new Error(error);

				var days = body.days;
				session.userData.days = days;
				var day_val = "days";
				if (days == "half" || days == 1){
					day_val = "day"
				}

				var options = { method: 'GET',
						url: config["backend_url"] + '/leavebalance',
						qs: { companyGUID: session.userData.companyGUID, userGUID:session.userData.userGUID, year: "2017" },
						headers: 
						{ 'cache-control': 'no-cache',
							'content-type': 'application/json' },
						json: true };

						request(options, function (error, response, body) {
							if (error) throw new Error(error);

							var leaveBalanceTable = body.leavebalance[0].Table;

							var leaveBalances = {};

							for (var i=0;i<leaveBalanceTable.length; i++){
								leaveBalances[leaveBalanceTable[i]['LeaveType']] = leaveBalanceTable[i]['Balance']
							}

							session.userData.leavebalance = leaveBalances;
							
							var leaveBalance = session.userData.leavebalance[session.userData.leaveType];
							if (days > leaveBalance){
								session.endDialog("Sorry you do not enough leave balance. You have " + leaveBalance + " days left of " + session.userData.leaveType + " leave.");
								session.userData.fromDate = undefined;
								session.userData.toDate   = undefined;
								session.beginDialog("/setDates");
							}else{
								if (session.userData.selectDates){
									builder.Prompts.choice(session,"That would be a "+ session.userData.leaveType +" leave for " + days +" " + day_val + ".Do you confirm your application?", "Yes|No");
									session.userData.selectDates = undefined;
								}else{
									var fromDateParts = session.userData.fromDate.split("-");
									var toDateParts   = session.userData.toDate.split("-");

									var fromDate = fromDateParts[1] + '-' + fromDateParts[0] + '-' + fromDateParts[2];
									var toDate   = toDateParts[1] + '-' + toDateParts[0] + '-' + toDateParts[2];						
									builder.Prompts.choice(session,"That would be a "+ session.userData.leaveType +" leave for " + days +" " + day_val +" starting from "+ fromDate +" to " + toDate +  ".Do you confirm your application?", "Yes|No");
								}
							}
						});
			});	
		}
	},
	function(session,results,next){
		var resp = results.response.entity.toLowerCase();

		if (resp == "yes" || resp == "yeah" || resp == "yay"){
			builder.Prompts.choice(session,"Do you want to add remarks", "Yes|No");
		} else{
			session.userData.leaveType = undefined;
			session.userData.fromDate = undefined;
			session.userData.toDate   = undefined;
			session.userData.fromTime = undefined;
			session.userData.toTime   = undefined;
			session.userData.date     = undefined;
			session.userData.indicator= undefined;
			session.endDialog("Okay, as you say :)");
		}
	},
	function(session,results,next){
		var resp = results.response.entity.toLowerCase();

		if (resp == "yes" || resp == "yeah" || resp == "yay"){
			builder.Prompts.text(session,"What remarks do you want to add?");
		} else{
			next();
		}
	},
	function(session,results){
		var resp = results.response;
		
		if (resp){
			session.userData.remarks = resp
		}else{
			session.userData.remarks = ""
		}
		
		var options = { method: 'POST',
		url: config["backend_url"] + '/leave',
		headers: 
		{ 	'cache-control': 'no-cache',
			'content-type': 'application/json' },
		body: 
		{   FromDate: session.userData.fromDate,
			ToDate: session.userData.toDate,
			FromTime: session.userData.fromTime,
			ToTime: session.userData.toTime,
			userGUID: session.userData.userGUID,
			companyGUID: session.userData.companyGUID,
			aoguid: session.userData.aoguid,
			roguid: session.userData.roguid,
			leaveday: session.userData.days,
			remarks: session.userData.remarks,
			cclist: '',
			leaveType: session.userData.leaveIds[session.userData.leaveType]},
		json: true };


		request(options, function (error, response, body) {
		if (error) throw new Error(error);
		session.userData.leaveguid = body.res;

		session.send("You have " + session.userData.days + " days of " + session.userData.leaveType + " pending leave.");

		session.userData.leaveType = undefined;
		session.userData.fromDate = undefined;
		session.userData.toDate   = undefined;
		session.userData.fromTime = undefined;
		session.userData.toTime   = undefined;
		session.userData.date     = undefined;
		session.userData.indicator= undefined;

		
		
		//session.userData.leavebalance[session.userData.leaveType] = 3;
		if (session.userData.aoname && session.userData.roname){
			session.endDialog("Your leave is now pending approval from " + session.userData.aoname + " and "+ session.userData.roname);
		}else if (!session.userData.aoname){
			session.endDialog("Your leave is now pending approval from " + session.userData.roname);
		}else if (!session.userData.roname){
			session.endDialog("Your leave is now pending approval from " + session.userData.aoname);
		}

		});
	}
]);

bot.dialog("/getTimes",[
	function(session){
		builder.Prompts.choice(session,"Choose from time","am|pm");
	},
	function(session,results){
		var resp = results.response.entity.toLowerCase();

		session.userData.fromTime = resp;
		builder.Prompts.choice(session,"Choose to time","am|pm");
	},
	function(session,results){
		var resp = results.response.entity.toLowerCase();

		session.userData.toTime = resp;
		session.endDialog();
		session.beginDialog("/confirmDates");
	}
]);

bot.dialog("/getFrom",[
	function(session){
		builder.Prompts.choice(session,"Choose to time","am|pm");
	},
	function(session,results){
		var resp = results.response.entity;

		session.userData.toTime = resp;

		builder.Prompts.text(session,"Till when do you want the leave?");
	},
	function(session,results){
		var resp = results.response;
		session.sendTyping();
		var request = app.textRequest(resp,{sessionId:session.message.user.id});

		request.on('response', function(response) {
			session.userData.date = undefined;
			session.userData.indicator = undefined;
			session.endDialog("getting from");
		});

		request.on('error', function(error){
			session.userData.date = undefined;
			session.userData.indicator = undefined;
			session.endDialog("getting from");
		});
}]);

bot.dialog("/getTo",[function(session){
	session.userData.date = undefined;
	session.userData.indicator = undefined;
	session.endDialog("getting to");
}]);


bot.dialog("/blank",function(session){
	session.endDialog();
})


//========================Send text message outside Microsoft Bot Framework===============//
function sendGenericMessage(recipientId, paystring) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      attachment: {
        type: "template",
        payload: {
          template_type: "generic",
          elements: [{
            title: "Dates",
            subtitle: paystring,
            buttons: [ {
              type: "postback",
              title: "Confirm Dates",
              payload: paystring,
            }]
          }]
        }
      }
    }
  };  

  callSendAPI(messageData);
}

function callSendAPI(messageData) {
  request({
    uri: 'https://graph.facebook.com/v2.6/me/messages',
    qs: { access_token: config["page_access_token"]},
    method: 'POST',
    json: messageData

  }, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      var recipientId = body.recipient_id;
      var messageId = body.message_id;
    } else {
      console.error("Unable to send message.");
      console.error(response);
      console.error(error);
    }
  });  
}


function getOverlapped(i=0,session){
	var options = { 
				method: 'GET',
				url: config["backend_url"] + '/overlapped',
				qs: 
				{   p_userGUID: session.userData.userGUID,
					p_FromDate: session.userData.fromDate,
					p_ToDate: session.userData.toDate,
					p_FromTime: session.userData.fromTime,
					p_ToTime: session.userData.toTime,
					p_LeaveType: session.userData.leaveIds[session.userData.leaveTypes[i]]},
				headers: 
				{ 'cache-control': 'no-cache' } 
			};

			request(options, function (error, response, body) {
				if (error) throw new Error(error);
				data = JSON.parse(body)
				
				if (data['isOverlapped'] == true){
					session.endDialog("Sorry you have already applied for " + session.userData.leaveTypes[i] + " leave on those days");
					session.beginDialog("/blank");
				}
				else if (i<session.userData.leaveTypes.length){
					i += 1;
					if (session.userData.leaveTypes[i]){
						getOverlapped(i,session);
					}else{
						session.userData.overlappedTest = true;
					}
				}
			});
}
//========================================================================================//