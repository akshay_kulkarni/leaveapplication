var builder = require('botbuilder');
var restify = require('restify');
var request = require('request');
var apiai   = require('apiai');
var apiairecognizer = require('api-ai-recognizer');

var app = apiai("36fb90fe7b7b4842a6bce96edaf46c86");

var fs = require('fs');
var config = JSON.parse(fs.readFileSync('config.json', 'utf8'));


var server = restify.createServer();
server.listen(process.env.PORT || process.env.port || 3978, function(){
    console.log('%s is listening to %s',server.name, server.url);
});


//=========Static File Serving and API Mock=======================================================================//
server.get(/\/static\/?.*/, restify.serveStatic({
  directory: __dirname
}));

server.get("/getDates", restify.queryParser(), function(req,res,next){
	if (req.query && req.query.fromDate && req.query.toDate && req.query.fromTime && req.query.toTime && req.query.psid){
		var psid     = req.query.psid;

		var fromDate = req.query.fromDate;
		var toDate   = req.query.toDate;
		var fromTime = req.query.fromTime;
		var toTime   = req.query.toTime;

		var timeStr = "from: " + fromDate + " " + fromTime + " to: " + toDate + " " + toTime;
		sendGenericMessage(psid, timeStr);
		res.send("Done");
	}else{
		res.send("Invalid");
	}
});

server.get('/authorize',restify.queryParser(),function(req,res,next){
	if (req.query && req.query.redirect_uri && req.query.username) {
    		var username = req.query.username;
			var company  = req.query.company;
			var password = req.query.password;

			var options = { method: 'POST',
			url: config["backend_url"] + '/authenticate',
			headers: 
			{ 'postman-token': '427625e2-dbd6-b74f-c914-dd6148986be9',
				'cache-control': 'no-cache',
				'content-type': 'application/json' },
			body: { companyId: company, username: username, password: password },
			json: true };

			request(options, function (error, response, body) {
				if (error) throw new Error(error);

				if (body.success)
				{
					var auth_code = username + "**" + company + "**" + body.userId; 
			
					var redirectUri = req.query.redirect_uri + '&authorization_code=' + auth_code;
					return res.redirect(redirectUri, next);
				} else{
					res.send("Invalid credentials");
				}
				
			});
	} else{
		return res.send(400, 'Request did not contain redirect_uri and username in the query string');
	}
});

// API Mocks

server.post('/authenticate',restify.bodyParser(), function(req,res){
	 data = req.body;

	 username = req.body.username;
	 companyId = req.body.companyId;
	 password  = req.body.password;

	 if (username == "akshay" && companyId=="ozz" && password=="password"){
		 var result = {success:true, userId:1};
	 } else if (username == "kwakc" && companyId=="justlogin" && password=="password"){
		 var result = {success:true,userId:3};
	 } else if (username == "vrsam" && companyId=="justlogin" && password=="password"){
		 var result = {success:true,userId:3};
	 } else{
		 var result = {success:false};
	 }
	 res.send(result);
});

//Get User Name
server.get('/usernameguid',restify.queryParser(),function(req,res){
	if (req.query && req.query.userid){
		var userId = req.query.userid;

		if (userId == 5){
			var result = {userName: "George", userGUID: "aabc"};
			res.send(result);
		} else if(userId == 4){
			var result = {userName: "Stephen", userGUID: "abcd"};
			res.send(result);
		} else {
			var result = {username: "Ian", userGUID:"acde"}
			res.send(result);
		}
	}else{
		res.send(400,"Invalid Request - User ID not supplied");
	}
});

// Get User Info
server.get('/userinfo',restify.queryParser(),function(req,res){
	if (req.query && req.query.userid){
		var userId = req.query.userid;

		if (userId == 1){
			var result = {userGUID:"aabc",roid:4,aoid:5,cclist:"a,b"};
			res.send(result);
		} else if(userId == 3){
			var result = {userGUID:"ccba",roid:4,aoid:5,cclist:"a,b"};
			res.send(result);
		} else {
			res.send(404,"Not found");
		}
	}else{
		res.send(400,"Invalid Request - User ID not supplied");
	}
});

// Get company GUID
server.get("/companyguid", restify.queryParser(), function(req,res){
	if (req.query && req.query.userGUID){
		var userGUID = req.query.userGUID;

		var result = {companyGUID:"aabc"};
		res.send(result);
	} else{
		res.send(400,"Invalid Request - User GUID not supplied");
	}
});

//get LeaveTypes
server.get("/leavetypes",restify.queryParser(), function(req,res){
	if (req.query && req.query.companyGUID && req.query.userGUID && req.query.year){
		var companyGUID = req.query.companyGUID;
		var userGUID = req.query.userGUID;
		var year = req.query.year;
		var result = {
						"types": [
							{
								"RowError": "",
								"RowState": 4,
								"Table": [
									{
										"leaveType": 1,
										"leavename": "Annual",
										"ifperincident": null
									},
									{
										"leaveType": 2,
										"leavename": "Examinations",
										"ifperincident": null
									},
									{
										"leaveType": 3,
										"leavename": "Maternity",
										"ifperincident": null
									},
									{
										"leaveType": 4,
										"leavename": "Sick",
										"ifperincident": null
									},
									{
										"leaveType": 5,
										"leavename": "Hospital",
										"ifperincident": null
									},
									{
										"leaveType": 6,
										"leavename": "No Pay",
										"ifperincident": null
									},
									{
										"leaveType": 7,
										"leavename": "Compassionate",
										"ifperincident": null
									},
									{
										"leaveType": 9,
										"leavename": "Paternity",
										"ifperincident": null
									},
									{
										"leaveType": 8,
										"leavename": "Others",
										"ifperincident": null
									}
								],
								"ItemArray": [
									1,
									"Annual",
									null
								],
								"HasErrors": false
							},
							{
								"RowError": "",
								"RowState": 4,
								"Table": [
									{
										"leaveType": 1,
										"leavename": "Annual",
										"ifperincident": null
									},
									{
										"leaveType": 2,
										"leavename": "Examinations",
										"ifperincident": null
									},
									{
										"leaveType": 3,
										"leavename": "Maternity",
										"ifperincident": null
									},
									{
										"leaveType": 4,
										"leavename": "Sick",
										"ifperincident": null
									},
									{
										"leaveType": 5,
										"leavename": "Hospital",
										"ifperincident": null
									},
									{
										"leaveType": 6,
										"leavename": "No Pay",
										"ifperincident": null
									},
									{
										"leaveType": 7,
										"leavename": "Compassionate",
										"ifperincident": null
									},
									{
										"leaveType": 9,
										"leavename": "Paternity",
										"ifperincident": null
									},
									{
										"leaveType": 8,
										"leavename": "Others",
										"ifperincident": null
									}
								],
								"ItemArray": [
									2,
									"Examinations",
									null
								],
								"HasErrors": false
							},
							{
								"RowError": "",
								"RowState": 4,
								"Table": [
									{
										"leaveType": 1,
										"leavename": "Annual",
										"ifperincident": null
									},
									{
										"leaveType": 2,
										"leavename": "Examinations",
										"ifperincident": null
									},
									{
										"leaveType": 3,
										"leavename": "Maternity",
										"ifperincident": null
									},
									{
										"leaveType": 4,
										"leavename": "Sick",
										"ifperincident": null
									},
									{
										"leaveType": 5,
										"leavename": "Hospital",
										"ifperincident": null
									},
									{
										"leaveType": 6,
										"leavename": "No Pay",
										"ifperincident": null
									},
									{
										"leaveType": 7,
										"leavename": "Compassionate",
										"ifperincident": null
									},
									{
										"leaveType": 9,
										"leavename": "Paternity",
										"ifperincident": null
									},
									{
										"leaveType": 8,
										"leavename": "Others",
										"ifperincident": null
									}
								],
								"ItemArray": [
									3,
									"Maternity",
									null
								],
								"HasErrors": false
							},
							{
								"RowError": "",
								"RowState": 4,
								"Table": [
									{
										"leaveType": 1,
										"leavename": "Annual",
										"ifperincident": null
									},
									{
										"leaveType": 2,
										"leavename": "Examinations",
										"ifperincident": null
									},
									{
										"leaveType": 3,
										"leavename": "Maternity",
										"ifperincident": null
									},
									{
										"leaveType": 4,
										"leavename": "Sick",
										"ifperincident": null
									},
									{
										"leaveType": 5,
										"leavename": "Hospital",
										"ifperincident": null
									},
									{
										"leaveType": 6,
										"leavename": "No Pay",
										"ifperincident": null
									},
									{
										"leaveType": 7,
										"leavename": "Compassionate",
										"ifperincident": null
									},
									{
										"leaveType": 9,
										"leavename": "Paternity",
										"ifperincident": null
									},
									{
										"leaveType": 8,
										"leavename": "Others",
										"ifperincident": null
									}
								],
								"ItemArray": [
									4,
									"Sick",
									null
								],
								"HasErrors": false
							},
							{
								"RowError": "",
								"RowState": 4,
								"Table": [
									{
										"leaveType": 1,
										"leavename": "Annual",
										"ifperincident": null
									},
									{
										"leaveType": 2,
										"leavename": "Examinations",
										"ifperincident": null
									},
									{
										"leaveType": 3,
										"leavename": "Maternity",
										"ifperincident": null
									},
									{
										"leaveType": 4,
										"leavename": "Sick",
										"ifperincident": null
									},
									{
										"leaveType": 5,
										"leavename": "Hospital",
										"ifperincident": null
									},
									{
										"leaveType": 6,
										"leavename": "No Pay",
										"ifperincident": null
									},
									{
										"leaveType": 7,
										"leavename": "Compassionate",
										"ifperincident": null
									},
									{
										"leaveType": 9,
										"leavename": "Paternity",
										"ifperincident": null
									},
									{
										"leaveType": 8,
										"leavename": "Others",
										"ifperincident": null
									}
								],
								"ItemArray": [
									5,
									"Hospital",
									null
								],
								"HasErrors": false
							},
							{
								"RowError": "",
								"RowState": 4,
								"Table": [
									{
										"leaveType": 1,
										"leavename": "Annual",
										"ifperincident": null
									},
									{
										"leaveType": 2,
										"leavename": "Examinations",
										"ifperincident": null
									},
									{
										"leaveType": 3,
										"leavename": "Maternity",
										"ifperincident": null
									},
									{
										"leaveType": 4,
										"leavename": "Sick",
										"ifperincident": null
									},
									{
										"leaveType": 5,
										"leavename": "Hospital",
										"ifperincident": null
									},
									{
										"leaveType": 6,
										"leavename": "No Pay",
										"ifperincident": null
									},
									{
										"leaveType": 7,
										"leavename": "Compassionate",
										"ifperincident": null
									},
									{
										"leaveType": 9,
										"leavename": "Paternity",
										"ifperincident": null
									},
									{
										"leaveType": 8,
										"leavename": "Others",
										"ifperincident": null
									}
								],
								"ItemArray": [
									6,
									"No Pay",
									null
								],
								"HasErrors": false
							},
							{
								"RowError": "",
								"RowState": 4,
								"Table": [
									{
										"leaveType": 1,
										"leavename": "Annual",
										"ifperincident": null
									},
									{
										"leaveType": 2,
										"leavename": "Examinations",
										"ifperincident": null
									},
									{
										"leaveType": 3,
										"leavename": "Maternity",
										"ifperincident": null
									},
									{
										"leaveType": 4,
										"leavename": "Sick",
										"ifperincident": null
									},
									{
										"leaveType": 5,
										"leavename": "Hospital",
										"ifperincident": null
									},
									{
										"leaveType": 6,
										"leavename": "No Pay",
										"ifperincident": null
									},
									{
										"leaveType": 7,
										"leavename": "Compassionate",
										"ifperincident": null
									},
									{
										"leaveType": 9,
										"leavename": "Paternity",
										"ifperincident": null
									},
									{
										"leaveType": 8,
										"leavename": "Others",
										"ifperincident": null
									}
								],
								"ItemArray": [
									7,
									"Compassionate",
									null
								],
								"HasErrors": false
							},
							{
								"RowError": "",
								"RowState": 4,
								"Table": [
									{
										"leaveType": 1,
										"leavename": "Annual",
										"ifperincident": null
									},
									{
										"leaveType": 2,
										"leavename": "Examinations",
										"ifperincident": null
									},
									{
										"leaveType": 3,
										"leavename": "Maternity",
										"ifperincident": null
									},
									{
										"leaveType": 4,
										"leavename": "Sick",
										"ifperincident": null
									},
									{
										"leaveType": 5,
										"leavename": "Hospital",
										"ifperincident": null
									},
									{
										"leaveType": 6,
										"leavename": "No Pay",
										"ifperincident": null
									},
									{
										"leaveType": 7,
										"leavename": "Compassionate",
										"ifperincident": null
									},
									{
										"leaveType": 9,
										"leavename": "Paternity",
										"ifperincident": null
									},
									{
										"leaveType": 8,
										"leavename": "Others",
										"ifperincident": null
									}
								],
								"ItemArray": [
									9,
									"Paternity",
									null
								],
								"HasErrors": false
							},
							{
								"RowError": "",
								"RowState": 4,
								"Table": [
									{
										"leaveType": 1,
										"leavename": "Annual",
										"ifperincident": null
									},
									{
										"leaveType": 2,
										"leavename": "Examinations",
										"ifperincident": null
									},
									{
										"leaveType": 3,
										"leavename": "Maternity",
										"ifperincident": null
									},
									{
										"leaveType": 4,
										"leavename": "Sick",
										"ifperincident": null
									},
									{
										"leaveType": 5,
										"leavename": "Hospital",
										"ifperincident": null
									},
									{
										"leaveType": 6,
										"leavename": "No Pay",
										"ifperincident": null
									},
									{
										"leaveType": 7,
										"leavename": "Compassionate",
										"ifperincident": null
									},
									{
										"leaveType": 9,
										"leavename": "Paternity",
										"ifperincident": null
									},
									{
										"leaveType": 8,
										"leavename": "Others",
										"ifperincident": null
									}
								],
								"ItemArray": [
									8,
									"Others",
									null
								],
								"HasErrors": false
							}
						]
					}
		res.send(result);
	}else{
		res.send(400,"Invalid request");
	}
});	

//get balance for a type
server.get("/leavebalance", restify.queryParser(), function(req,res){
	if (req.query && req.query.companyGUID && req.query.userGUID && req.query.year){
		var result = {
						"leavebalance": [
							{
								"RowError": "",
								"RowState": 4,
								"Table": [
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "1",
										"LeaveType": "Annual",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "2",
										"LeaveType": "Examinations",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "3",
										"LeaveType": "Maternity",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "4",
										"LeaveType": "Sick",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "5",
										"LeaveType": "Hospital",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "6",
										"LeaveType": "No Pay",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "7",
										"LeaveType": "Compassionate",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "9",
										"LeaveType": "Paternity",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "8",
										"LeaveType": "Others",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									}
								],
								"ItemArray": [
									"\"abc\"",
									"\"123\"",
									"1",
									"Annual",
									"1/1/1900 12:00:00 AM",
									"0",
									"0",
									"0",
									"0",
									"0",
									"1/1/1900 12:00:00 AM",
									"",
									"",
									""
								],
								"HasErrors": false
							},
							{
								"RowError": "",
								"RowState": 4,
								"Table": [
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "1",
										"LeaveType": "Annual",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "2",
										"LeaveType": "Examinations",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "3",
										"LeaveType": "Maternity",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "4",
										"LeaveType": "Sick",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "5",
										"LeaveType": "Hospital",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "6",
										"LeaveType": "No Pay",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "7",
										"LeaveType": "Compassionate",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "9",
										"LeaveType": "Paternity",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "8",
										"LeaveType": "Others",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									}
								],
								"ItemArray": [
									"\"abc\"",
									"\"123\"",
									"2",
									"Examinations",
									"1/1/1900 12:00:00 AM",
									"0",
									"0",
									"0",
									"0",
									"0",
									"1/1/1900 12:00:00 AM",
									"",
									"",
									""
								],
								"HasErrors": false
							},
							{
								"RowError": "",
								"RowState": 4,
								"Table": [
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "1",
										"LeaveType": "Annual",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "2",
										"LeaveType": "Examinations",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "3",
										"LeaveType": "Maternity",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "4",
										"LeaveType": "Sick",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "5",
										"LeaveType": "Hospital",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "6",
										"LeaveType": "No Pay",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "7",
										"LeaveType": "Compassionate",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "9",
										"LeaveType": "Paternity",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "8",
										"LeaveType": "Others",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									}
								],
								"ItemArray": [
									"\"abc\"",
									"\"123\"",
									"3",
									"Maternity",
									"1/1/1900 12:00:00 AM",
									"0",
									"0",
									"0",
									"0",
									"0",
									"1/1/1900 12:00:00 AM",
									"",
									"",
									""
								],
								"HasErrors": false
							},
							{
								"RowError": "",
								"RowState": 4,
								"Table": [
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "1",
										"LeaveType": "Annual",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "2",
										"LeaveType": "Examinations",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "3",
										"LeaveType": "Maternity",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "4",
										"LeaveType": "Sick",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "5",
										"LeaveType": "Hospital",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "6",
										"LeaveType": "No Pay",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "7",
										"LeaveType": "Compassionate",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "9",
										"LeaveType": "Paternity",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "8",
										"LeaveType": "Others",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									}
								],
								"ItemArray": [
									"\"abc\"",
									"\"123\"",
									"4",
									"Sick",
									"1/1/1900 12:00:00 AM",
									"0",
									"0",
									"0",
									"0",
									"0",
									"1/1/1900 12:00:00 AM",
									"",
									"",
									""
								],
								"HasErrors": false
							},
							{
								"RowError": "",
								"RowState": 4,
								"Table": [
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "1",
										"LeaveType": "Annual",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "2",
										"LeaveType": "Examinations",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "3",
										"LeaveType": "Maternity",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "4",
										"LeaveType": "Sick",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "5",
										"LeaveType": "Hospital",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "6",
										"LeaveType": "No Pay",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "7",
										"LeaveType": "Compassionate",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "9",
										"LeaveType": "Paternity",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "8",
										"LeaveType": "Others",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									}
								],
								"ItemArray": [
									"\"abc\"",
									"\"123\"",
									"5",
									"Hospital",
									"1/1/1900 12:00:00 AM",
									"0",
									"0",
									"0",
									"0",
									"0",
									"1/1/1900 12:00:00 AM",
									"",
									"",
									""
								],
								"HasErrors": false
							},
							{
								"RowError": "",
								"RowState": 4,
								"Table": [
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "1",
										"LeaveType": "Annual",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "2",
										"LeaveType": "Examinations",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "3",
										"LeaveType": "Maternity",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "4",
										"LeaveType": "Sick",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "5",
										"LeaveType": "Hospital",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "6",
										"LeaveType": "No Pay",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "7",
										"LeaveType": "Compassionate",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "9",
										"LeaveType": "Paternity",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "8",
										"LeaveType": "Others",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									}
								],
								"ItemArray": [
									"\"abc\"",
									"\"123\"",
									"6",
									"No Pay",
									"1/1/1900 12:00:00 AM",
									"0",
									"0",
									"0",
									"0",
									"0",
									"1/1/1900 12:00:00 AM",
									"",
									"",
									""
								],
								"HasErrors": false
							},
							{
								"RowError": "",
								"RowState": 4,
								"Table": [
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "1",
										"LeaveType": "Annual",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "2",
										"LeaveType": "Examinations",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "3",
										"LeaveType": "Maternity",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "4",
										"LeaveType": "Sick",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "5",
										"LeaveType": "Hospital",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "6",
										"LeaveType": "No Pay",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "7",
										"LeaveType": "Compassionate",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "9",
										"LeaveType": "Paternity",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "8",
										"LeaveType": "Others",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									}
								],
								"ItemArray": [
									"\"abc\"",
									"\"123\"",
									"7",
									"Compassionate",
									"1/1/1900 12:00:00 AM",
									"0",
									"0",
									"0",
									"0",
									"0",
									"1/1/1900 12:00:00 AM",
									"",
									"",
									""
								],
								"HasErrors": false
							},
							{
								"RowError": "",
								"RowState": 4,
								"Table": [
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "1",
										"LeaveType": "Annual",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "2",
										"LeaveType": "Examinations",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "3",
										"LeaveType": "Maternity",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "4",
										"LeaveType": "Sick",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "5",
										"LeaveType": "Hospital",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "6",
										"LeaveType": "No Pay",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "7",
										"LeaveType": "Compassionate",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "9",
										"LeaveType": "Paternity",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "8",
										"LeaveType": "Others",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									}
								],
								"ItemArray": [
									"\"abc\"",
									"\"123\"",
									"9",
									"Paternity",
									"1/1/1900 12:00:00 AM",
									"0",
									"0",
									"0",
									"0",
									"0",
									"1/1/1900 12:00:00 AM",
									"",
									"",
									""
								],
								"HasErrors": false
							},
							{
								"RowError": "",
								"RowState": 4,
								"Table": [
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "1",
										"LeaveType": "Annual",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "2",
										"LeaveType": "Examinations",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "3",
										"LeaveType": "Maternity",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "4",
										"LeaveType": "Sick",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "5",
										"LeaveType": "Hospital",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "6",
										"LeaveType": "No Pay",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "7",
										"LeaveType": "Compassionate",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "9",
										"LeaveType": "Paternity",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									},
									{
										"UserGUID": "\"abc\"",
										"CompnanyGUID": "\"123\"",
										"LeaveTypeID": "8",
										"LeaveType": "Others",
										"BasedOnDate": "1/1/1900 12:00:00 AM",
										"Balance": "0",
										"Entitlement": "0",
										"LBF": "0",
										"Taken": "0",
										"Adjust": "0",
										"JoinedDate": "1/1/1900 12:00:00 AM",
										"Dept": "",
										"EmpID": "",
										"FullName": ""
									}
								],
								"ItemArray": [
									"\"abc\"",
									"\"123\"",
									"8",
									"Others",
									"1/1/1900 12:00:00 AM",
									"0",
									"0",
									"0",
									"0",
									"0",
									"1/1/1900 12:00:00 AM",
									"",
									"",
									""
								],
								"HasErrors": false
							}
						]
					}
		res.send(result);
	}else{
		res.send(400,"Invalid request")
	}
});

//get number of days
server.get("/days", restify.queryParser(), function(req,res){
 if (req.query && req.query.fromDate && req.query.fromTime && req.query.toDate && req.query.toTime && req.query.userGUID && req.query.companyGUID){
	 var fromDate = new Date(req.query.fromDate);
	 var toDate   = new Date(req.query.toDate);
	 var fromTime = req.query.fromTime;
	 var toTime   = req.query.toTime;
	 var userGUID = req.query.userGUID;
	 var companyGUID = req.query.companyGUID;

	 var oneDay = 24*60*60*1000;

	 var diffDays = Math.round(Math.abs((fromDate.getTime() - toDate.getTime())/(oneDay))) + 1;

	 console.log('here');
	 console.log(diffDays);

	 if (diffDays == 1){
		 if (fromTime == toTime){
			 diffDays = "half"
		 }
	 }

	 var result = {"days":diffDays};
	 res.send(result);
 }else{
	 res.send(400,"Invalid request");
 }
});

//apply for a leave
server.post("/leave", restify.bodyParser(), function(req,res){
	data = req.body;

	var fromDate = new Date(data.fromDate);
	var toDate   = new Date(data.toDate);
	var fromTime = data.fromTime;
	var toTime   = data.toTime;
	var userGUID = data.userGUID;
	var companyGUID = data.companyGUID;
	var aoguid = data.aoguid;
	var roguid = data.roguid;
	var days   = data.days;
	var remarks = data.remarks;
	var cclist = data.cclist;
	var leaveType = data.leaveType;


	var byGuid = userGUID;
	var createdDate = Date.now();

	if (days == 1){
		var result = {"message":"Applied for " + leaveType + " leave for 1 day"};
	} else{
		var result = {"message":"Applied for " + leaveType + " leave for " + days + " days"};
	}

	
	res.send(result);
});
//====================================================================================================//



var connector = new builder.ChatConnector({
    appId: config["microsoft_app_id"],
    appPassword: config["microsoft_app_secret"]
});


var bot = new builder.UniversalBot(connector);
server.post('/api/messages',connector.listen());

var recognizer = new apiairecognizer('70cbdadb7d184803a88c6f8f56ea5196');
var intents = new builder.IntentDialog({
	recognizers:[recognizer]
});

bot.dialog('/', intents);


intents.matches('reset', function(session, args){
	session.send("done");
	session.userData = {};
});

intents.matches('smalltalk.greetings.hello',function(session,args){
	session.send("Hey " + session.message.user.name);
	var replyMessage = new builder.Message(session)
                                            .text("I can help you apply for leave");

	replyMessage.sourceEvent({ 
				facebook: { 
					quick_replies: [{
						content_type:"text",
						title:"Apply for leave",
						payload:"apply for leave"
					},
					{
						content_type:"text",
						title:"Check leave balance",
						payload:"check leave balance"
					}]
				}
			});
    session.send(replyMessage);
});

intents.matches('smalltalk.appraisal.thank_you', function(session,args){
	var fulfillment =  builder.EntityRecognizer.findEntity(args.entities, 'fulfillment');
	if (fulfillment){
			var speech = fulfillment.entity;
			session.send(speech);
	}else{
		session.send('Sorry...not sure how to respond to that');
	}
});

intents.matches('smalltalk.greetings.goodmorning', function(session,args){
	var fulfillment =  builder.EntityRecognizer.findEntity(args.entities, 'fulfillment');
	if (fulfillment){
			var speech = fulfillment.entity;
			session.send(speech);
	}else{
		session.send('Sorry...not sure how to respond to that');
	}
});

intents.matches('login',function(session){
	session.beginDialog("/login")
});

intents.matches('type', function(session,args){
	if (session.userData.companyId && session.userData.companyId != 'undefined'){
		var type = builder.EntityRecognizer.findEntity(args.entities,"leaveTypes");
		if (type){
			type = type.entity;
			session.userData.leaveType1 = type;
		}
		session.beginDialog("/setType");
	}else{
		session.send("You need to log into your justlogin account before proceeding");
		session.userData.lastAction = "applyLeave";
		session.beginDialog("/login");
	}
});

intents.matches('dates',function(session){
	if (session.userData.companyId && session.userData.companyId != 'undefined'){
		session.beginDialog("/setDates");
	}else{
		session.send("You need to log into your justlogin account before proceeding");
		session.userData.lastAction = "applyLeave";
		session.beginDialog("/login");
	}
});

intents.matches('todayLeave', function(session, args){
	session.userData.leaveQuestion = "apply";
	if (session.userData.companyId && session.userData.companyId != 'undefined'){
		var leaveType = builder.EntityRecognizer.findEntity(args.entities, 'leaveTypes');
		var date = builder.EntityRecognizer.findEntity(args.entities,'date');

		var fromDateParts = date.entity.split('-');

		var fromDate = fromDateParts[1] + "-" + fromDateParts[2] + "-" + fromDateParts[0];
		var toDate = fromDate;

		session.userData.fromDate = fromDate;//datePeriod[0];
		session.userData.toDate = toDate;//datePeriod

		if (leaveType){
			leaveType = leaveType.entity;

			if (session.userData.leaveTypes.indexOf(leaveType)<=-1){
				session.endDialog("Sorry you don't have that type of leave available");
			}
			session.userData.leaveType = leaveType;
		}
		session.beginDialog("/applyLeave");
	}else{
		session.send("You need to log into your justlogin account before proceeding");
		session.userData.lastAction = "applyLeave";
		session.beginDialog("/login");
	}
});

intents.matches('leaveApply',function(session,args){
	session.userData.leaveQuestion = "apply";
	if (session.userData.companyId && session.userData.companyId != 'undefined'){
		var leaveType = builder.EntityRecognizer.findEntity(args.entities, 'leaveTypes');
		var date = builder.EntityRecognizer.findEntity(args.entities,'date');
		var datePeriod = builder.EntityRecognizer.findEntity(args.entities, 'date-period');
		var indicator = builder.EntityRecognizer.findEntity(args.entities,'indicator');
		if(datePeriod){
			datePeriod = datePeriod.entity.split("/");
			if (datePeriod.length == 2)
			{
				var fromDateParts = datePeriod[0].split('-');
				var toDateParts   = datePeriod[1].split('-');

				var fromDate = fromDateParts[1] + "-" + fromDateParts[2] + "-" + fromDateParts[0];
				var toDate = toDateParts[1] + "-" + toDateParts[2] + "-" + toDateParts[0];
				session.userData.fromDate = fromDate;//datePeriod[0];
				session.userData.toDate = toDate;//datePeriod[1];
			}
		} else if(indicator){
			indicator = indicator.entity;
			session.userData.indicator = indicator
		}
		if (date){
			date = date.entity;
			if (date.constructor == Array){
				var fromDateParts = date[0].split('-');
				var toDateParts   = date[1].split('-');

				var fromDate = fromDateParts[1] + "-" + fromDateParts[2] + "-" + fromDateParts[0];
				var toDate = toDateParts[1] + "-" + toDateParts[2] + "-" + toDateParts[0];
				session.userData.fromDate = fromDate;//datePeriod[0];
				session.userData.toDate = toDate;//datePeriod[1];
			}else{
				session.userData.date = date;
			}
		}
		if (leaveType){
			leaveType = leaveType.entity;

			if (session.userData.leaveTypes.indexOf(leaveType)<=-1){
				session.endDialog("Sorry you don't have that type of leave available");
			}
			session.userData.leaveType = leaveType;
		}
		console.log('here');
		session.beginDialog("/applyLeave");
	}else{
		session.send("You need to log into your justlogin account before proceeding");
		session.userData.lastAction = "applyLeave";
		session.beginDialog("/login");
	}
	
});

intents.matches('checkBalance',function(session, args){
	session.userData.leaveQuestion = "balance";
	if (session.userData.companyId && session.userData.companyId != 'undefined'){
		var leaveType = builder.EntityRecognizer.findEntity(args.entities,"leaveTypes")
		if (leaveType){
			leaveType = leaveType.entity;
			session.userData.leaveType = leaveType;
		}
		session.beginDialog("/checkBalance");
	}else{
		session.send("You need to log into your justlogin account before proceeding");
		session.userData.lastAction = "checkBalance";
		session.beginDialog("/login");
	}
});


intents.matches('logout',
  function (session) {
    request({
      url: 'https://graph.facebook.com/v2.6/me/unlink_accounts',
      method: 'POST',
      qs: {
        access_token: config["page_access_token"]
      },
      body: {
        psid: session.message.address.user.id
      },
      json: true
    }, function (error, response, body) {
      if (!error && response.statusCode === 200) {
        // No need to do anything send anything to the user
        // in the success case since we respond only after
        // we have received the account unlinking webhook
        // event from Facebook.
        session.endDialog();
      } else {
        session.endDialog('Error while unlinking account');
      }
    });
  }
);

intents.matches('handleDates', function(session, args){
	if (session.userData.companyId && session.userData.companyId != 'undefined'){
		var dates = builder.EntityRecognizer.findEntity(args.entities, 'date-time').entity;

		var fromDate = dates[0]
		var toDate   = dates[1]

		var fromTime = builder.EntityRecognizer.findEntity(args.entities, 'time').entity;
		var toTime = builder.EntityRecognizer.findEntity(args.entities, 'time1').entity;

		session.userData.fromDate = fromDate;
		session.userData.toDate = toDate;
		session.userData.fromTime = fromTime;
		session.userData.toTime = toTime;

		session.userData.selectDates = true;

		session.beginDialog('/confirmDates');
	}else{
		session.send("You need to log into your justlogin account before proceeding");
		session.userData.lastAction = "applyLeave";
		session.beginDialog("/login");
	}
});

intents.onDefault(function(session){
	var accountLinking = session.message.sourceEvent.account_linking;
	if (accountLinking){
		var authorizationStatus = accountLinking.status;
		if (authorizationStatus === 'linked') {
			session.sendTyping();
			var data = accountLinking.authorization_code.split("**");
			var username = data[0];
			var company  = data[1];
			var userId   = data[2];
			// Persist username under the userData
			session.userData.username = username;
			session.userData.companyId = company;
			session.userData.userId = userId;
			
			
			//==========GetuserInfo=================//
				var options = { method: 'GET',
				url: config["backend_url"] + '/userinfo',
				qs: { userid: userId },
				headers: 
				{ 'cache-control': 'no-cache',
				  'content-type': 'application/json' },
				json: true };

				request(options, function (error, response, body) {
					if (error) throw new Error(error);

					session.userData.userGUID = body.userGUID;
					session.userData.roid = body.roid;
					session.userData.aoid = body.aoid;
					session.userData.cclist = body.cclist;

					// console.log({"userGUID" : session.userData.userGUID});
					// console.log({"roid": session.userData.roid});
					// console.log({"aoid": session.userData.aoid});
					// console.log({"cclist": session.userData.cclist});

					//==========GetAORO Info=================//
					if (session.userData.aoid){
						var options = { method: 'GET',
						url: config["backend_url"] + '/usernameguid',
						qs: { userid: session.userData.aoid },
						headers: 
						{ 'postman-token': 'd0a15f27-e1bd-cce7-a586-b97f136d40bc',
							'cache-control': 'no-cache',
							'content-type': 'application/json' },
						json: true };

						request(options, function (error, response, body) {
							if (error) throw new Error(error);

							session.userData.aoName = body.userName;
							session.userData.aoGUID = body.userGUID;

							//console.log({"aoName":session.userData.aoName,"aoGUID":session.userData.aoGUID});
						});
					}

					if (session.userData.roid){
						var options = { method: 'GET',
						url: config["backend_url"] + '/usernameguid',
						qs: { userid: session.userData.roid },
						headers: 
						{ 'postman-token': 'd0a15f27-e1bd-cce7-a586-b97f136d40bc',
							'cache-control': 'no-cache',
							'content-type': 'application/json' },
						json: true };

						request(options, function (error, response, body) {
							if (error) throw new Error(error);

							session.userData.roName = body.userName;
							session.userData.roGUID = body.userGUID;

							//console.log({"roName":session.userData.roName,"roGUID":session.userData.roGUID});
						});
					}

					//==========GetCompanyGUID==============//
					var options = { method: 'GET',
					url: config["backend_url"] + '/companyguid',
					qs: { userGUID: session.userData.userGUID},
					headers: 
					{   'cache-control': 'no-cache',
						'content-type': 'application/json' },
					json: true };

					request(options, function (error, response, body) {
						if (error) throw new Error(error);

						session.userData.companyGUID = body.companyGUID;
						//console.log({"companyGUID":session.userData.companyGUID});

						//===========GetLeaveTypes================//
						var options = { method: 'GET',
						url: config["backend_url"] + '/leavetypes',
						qs: { companyGUID: session.userData.companyGUID, userGUID:session.userData.userGUID, year: "2017" },
						headers: 
						{ 	'cache-control': 'no-cache',
							'content-type': 'application/json' },
						json: true };

						request(options, function (error, response, body) {
							if (error) throw new Error(error);

							var leaveTypes = [];
							var leaveIds = {};
							var leaveTypesTable = body.types[0].Table;

							for (var i=0;i<leaveTypesTable.length; i++){
								leaveTypes.push(leaveTypesTable[i]['leavename']);
								leaveIds[leaveTypesTable[i]['leavename']] = leaveTypesTable[i]['leaveType']
							}

							session.userData.leaveTypes = leaveTypes;
							session.userData.leaveIds = leaveIds;
							//console.log({"leavetypes":session.userData.leaveTypes,"leaveids":session.userData.leaveIds});
						});

						//========================================//

						//==========GetLeaveBalances==============//
						var options = { method: 'GET',
						url: config["backend_url"] + '/leavebalance',
						qs: { companyGUID: session.userData.companyGUID, userGUID:session.userData.userGUID, year: "2017" },
						headers: 
						{ 'cache-control': 'no-cache',
						'content-type': 'application/json' },
						json: true };

						request(options, function (error, response, body) {
							if (error) throw new Error(error);
							var leaveBalanceTable = body.leavebalance[0].Table;
							var leaveBalances = {};

							for (var i=0;i<leaveBalanceTable.length; i++){
								leaveBalances[leaveBalanceTable[i]['LeaveType']] = leaveBalanceTable[i]['Balance']
							}

							session.userData.leaveBalance = leaveBalances;

							console.log(session.userData);
							// var lastAction = session.userData.lastAction;

							// if (lastAction == 'checkBalance'){
							// 	session.beginDialog("/checkBalance");
							// }else if (lastAction == 'applyLeave'){
							// 	session.beginDialog("/applyLeave");
							// }
						});
					});
				});
		}else if (authorizationStatus === 'unlinked') {
			// Remove username from the userData
			session.userData.companyId = undefined;
			session.userData.username = undefined;
			session.userData.lastAction = undefined;
			session.userData.leaveTypes = undefined;
			session.userData.leaveType = undefined;
			session.userData.roGUID = undefined;
			session.userData.aoGUID = undefined;
			session.userData.roName = undefined;
			session.userData.aoName = undefined;  
			session.userData.cclist = undefined;
			session.userData.aoid   = undefined;
			session.userData.roid   = undefined;
			session.userData.userGUID = undefined;
			session.userData.companyGUID = undefined;
			session.userData.userId = undefined;

			session.endDialog();
		} else{
			session.endDialog('Unknown account linking event received');
		}
	}else{
		session.send("Sorry...I couldn't understand");
		var replyMessage = new builder.Message(session)
                                            .text("I can help you apply for leave");

		replyMessage.sourceEvent({ 
				facebook: { 
					quick_replies: [{
						content_type:"text",
						title:"Apply for leave",
						payload:"apply for leave"
					},
					{
						content_type:"text",
						title:"Check leave balance",
						payload:"check leave balance"
					}]
				}
			});
		session.send(replyMessage);
		}
});

bot.dialog('/login',function(session){
	var message = new builder.Message(session)
      .sourceEvent({
        facebook: {
          attachment: {
            type: 'template',
            payload: {
              template_type: 'generic',
              elements: [{
                title: 'Log into your justlogin account',
                image_url: 'https://res.cloudinary.com/crunchbase-production/image/upload/v1470360294/jvo3fcpicbmxkyjuvyio.png',
                buttons: [{
                  type: 'account_link',
                  url: config["frontend_url"] + '/static/index.html'
                }]
              }]
            }
          }
        }
      });
    session.endDialog(message);
});

bot.dialog('/setDates',[
function(session){
	if(session.userData.fromDate && session.userData.toDate){
		session.beginDialog("/getTimes");
	}
	// else if(session.userData.date){
	// 	if (session.userData.indicator == "to"){
	// 		session.beginDialog("/getFrom");
	// 	}else{
	// 		session.beginDialog("/getTo");
	// 	}
	// }
	else{
		if (!session.userData.leaveType){
			session.userData.leaveType = "";
		}
		var fb_id = session.message.user.id;
		var message = new builder.Message(session)
		.sourceEvent({
			facebook: {
			attachment: {
				type: 'template',
				payload: {
				template_type: 'button',
				text:'Please select the from and to dates for ' + session.userData.leaveType + ' leave.',
				buttons:[
					{
						"type":"web_url",
						"url":config["frontend_url"] + "/static/dates.html?psid=" + session.message.user.id,
						"title":"Select Dates",
						"messenger_extensions": true
					}
				]
				}
			}
			}
		});
		session.endDialog(message);
	}
}]);

bot.dialog('/setType', [
	function(session){
		if (!session.userData.leaveType1){
			var types = session.userData.leaveTypes.join('|');
			builder.Prompts.choice(session,"Choose leave type?",types);
		}else{
			session.userData.leaveType = session.userData.leaveType1;
			session.userData.leaveType1 = undefined;
			session.endDialog();
			if (session.userData.leaveQuestion){
				if (session.userData.leaveQuestion == "apply"){
					session.beginDialog("/applyLeave");
				}else{
					session.beginDialog("/checkBalance");
				}
			}else{
				session.beginDialog("/applyLeave");
			}
		}
},
	function(session,results){
		session.userData.leaveType = results.response.entity;
		if (session.userData.leaveQuestion){
			if (session.userData.leaveQuestion == "apply"){
				session.beginDialog("/applyLeave");
			}else{
				session.beginDialog("/checkBalance");
			}
		} else{
			session.beginDialog("/applyLeave");
		}
		
		session.endDialog();
	}
]);

bot.dialog("/applyLeave", [
	function(session){
		if (!session.userData.leaveType || session.userData.leaveType == "undefined"){
			console.log("userData");
			console.log(session.userData);
			session.endDialog();
			// var types = session.userData.leaveTypes.join('|');
			// builder.Prompts.choice(session,"Choose leave type?",types);
		}else{
			session.endDialog();
			session.beginDialog("/setDates");
		}	
},
	function(session,results){
		if (!session.userData.leaveType || session.userData.leaveType == "undefined"){
			session.userData.leaveType = results.response.entity;
			session.endDialog();
			session.beginDialog("/setDates");
		} else{
			session.endDialog();
		}
	}
]);

bot.dialog("/checkBalance", [
	function(session){
		if (session.userData.leaveType){
			var leaveBalance = session.userData.leavebalance[session.userData.leaveType];
			session.endDialog("You have " + leaveBalance + " days of " + session.userData.leaveType + " leave left");	
		}else{
			session.endDialog();
			session.beginDialog("/setType")
		}	
}]);

bot.dialog("/confirmDates",[
	function(session){
		if (new Date(session.userData.fromDate) > new Date(session.userData.toDate)){
			session.endDialog("From date has to be before to date");
			session.userData.fromDate = undefined;
			session.userData.toDate = undefined;
			session.beginDialog("/setDates");
		}else{
			var options = { method: 'GET',
			url: config["backend_url"] + '/days',
			qs: 
			{ fromDate: session.userData.fromDate,
				toDate: session.userData.toDate,
				fromTime: session.userData.fromTime,
				toTime: session.userData.toTime,
				userGUID: session.userData.userGUID,
				companyGUID: session.userData.companyGUID },
			headers: 
			{ 'postman-token': '16826e3f-7c8c-932a-cdaa-6289dadbd2f5',
				'cache-control': 'no-cache',
				'content-type': 'application/json' },
			json: true };

			request(options, function (error, response, body) {
				if (error) throw new Error(error);

				var days = body.days;
				session.userData.days = days;
				var day_val = "days";
				if (days == "half" || days == 1){
					day_val = "day"
				}

				var leaveBalance = session.userData.leavebalance[session.userData.leaveType];

				if (days > leaveBalance){
					console.log(days);
					session.endDialog("Sorry you do not enough leave balance. You have " + leaveBalance + " days left of " + session.userData.leaveType + " leave.");
					session.userData.fromDate = undefined;
					session.userData.toDate   = undefined;
					session.beginDialog("/setDates");
				}else{
					if (session.userData.selectDates){
						builder.Prompts.choice(session,"That would be a "+ session.userData.leaveType +" leave for " + days +" " + day_val + ".Do you confirm your application?", "Yes|No");
						session.userData.selectDates = undefined;
					}else{
						var fromDateParts = session.userData.fromDate.split("-");
						var toDateParts   = session.userData.toDate.split("-");

						var fromDate = fromDateParts[1] + '-' + fromDateParts[0] + '-' + fromDateParts[2];
						var toDate   = toDateParts[1] + '-' + toDateParts[0] + '-' + toDateParts[2];	
						if (days == 'half'){
							builder.Prompts.choice(session,"That would be a "+ session.userData.leaveType +" leave for " + days +" " + day_val +" on "+ fromDate +" from " + session.userData.fromTime + " to " + session.userData.toTime +  ".Do you confirm your application?", "Yes|No");
						}else{
							builder.Prompts.choice(session,"That would be a "+ session.userData.leaveType +" leave for " + days +" " + day_val +" starting from "+ fromDate + " " + session.userData.fromTime +" to " + toDate + " " + session.userData.toTime + ".Do you confirm your application?", "Yes|No");
						}				
					}
					
				}
			});	
		}
	},
	function(session,results,next){
		var resp = results.response.entity.toLowerCase();

		if (resp == "yes" || resp == "yeah" || resp == "yay"){
			builder.Prompts.choice(session,"Do you want to add remarks", "Yes|No");
		} else{
			session.userData.leaveType = undefined;
			session.userData.fromDate = undefined;
			session.userData.toDate   = undefined;
			session.userData.fromTime = undefined;
			session.userData.toTime   = undefined;
			session.userData.date     = undefined;
			session.userData.indicator= undefined;
			session.endDialog("Okay, as you say :)");
		}
	},
	function(session,results,next){
		var resp = results.response.entity.toLowerCase();

		if (resp == "yes" || resp == "yeah" || resp == "yay"){
			builder.Prompts.text(session,"What remarks do you want to add?");
		} else{
			next();
		}
	},
	function(session,results){
		var resp = results.response;
		
		if (resp){
			session.userData.remarks = resp
		}else{
			session.userData.remarks = ""
		}
		
		var options = { method: 'POST',
		url: config["backend_url"] + '/leave',
		headers: 
		{ 'postman-token': 'ba2d5761-3a1a-3eea-d4ae-cd103ddd1738',
			'cache-control': 'no-cache',
			'content-type': 'application/json' },
		body: 
		{   fromDate: session.userData.fromDate,
			toDate: session.userData.toDate,
			fromTime: session.userData.fromTime,
			toTime: session.userData.toTime,
			userGUID: session.userData.userGUID,
			companyGUID: session.userData.companyGUID,
			aoguid: session.userData.aoguid,
			roguid: session.userData.roguid,
			days: session.userData.days,
			remarks: session.userData.remarks,
			cclist: session.userData.cclist,
			leaveType: session.userData.leaveType },
		json: true };

		request(options, function (error, response, body) {
		if (error) throw new Error(error);


		if (session.userData.days == 'half'){
			session.userData.leavebalance[session.userData.leaveType] -= 0.5;
		}else{
			session.userData.leavebalance[session.userData.leaveType] -= session.userData.days;
		}
		var days= session.userData.leavebalance[session.userData.leaveType] 

		
		session.send(body.message);

		session.send("You have " + days + " days of " + session.userData.leaveType + " left.");

		session.userData.leaveType = undefined;
		session.userData.fromDate = undefined;
		session.userData.toDate   = undefined;
		session.userData.fromTime = undefined;
		session.userData.toTime   = undefined;
		session.userData.date     = undefined;
		session.userData.indicator= undefined;

		
		
		//session.userData.leavebalance[session.userData.leaveType] = 3;
		if (session.userData.aoName && session.userData.roName){
			session.endDialog("Your leave is now pending approval from " + session.userData.aoName + " and "+ session.userData.roName);
		}else if (!session.userData.aoName){
			session.endDialog("Your leave is now pending approval from " + session.userData.roName);
		}else if (!session.userData.roName){
			session.endDialog("Your leave is now pending approval from " + session.userData.aoName);
		}

		});
	}
]);

bot.dialog("/getTimes",[
	function(session){
		builder.Prompts.choice(session,"Choose from time","am|pm");
	},
	function(session,results){
		var resp = results.response.entity.toLowerCase();

		session.userData.fromTime = resp;
		builder.Prompts.choice(session,"Choose to time","am|pm");
	},
	function(session,results){
		var resp = results.response.entity.toLowerCase();

		session.userData.toTime = resp;
		session.endDialog();
		session.beginDialog("/confirmDates");
	}
]);

bot.dialog("/getFrom",[
	function(session){
		builder.Prompts.choice(session,"Choose to time","am|pm");
	},
	function(session,results){
		var resp = results.response.entity;

		session.userData.toTime = resp;

		builder.Prompts.text(session,"Till when do you want the leave?");
	},
	function(session,results){
		var resp = results.response;
		session.sendTyping();
		var request = app.textRequest(resp,{sessionId:session.message.user.id});

		request.on('response', function(response) {
			session.userData.date = undefined;
			session.userData.indicator = undefined;
			session.endDialog("getting from");
		});

		request.on('error', function(error){
			session.userData.date = undefined;
			session.userData.indicator = undefined;
			session.endDialog("getting from");
			console.log(error);
		});
}]);

bot.dialog("/getTo",[function(session){
	session.userData.date = undefined;
	session.userData.indicator = undefined;
	session.endDialog("getting to");
}]);


//========================Send text message outside Microsoft Bot Framework===============//
function sendGenericMessage(recipientId, paystring) {
  var messageData = {
    recipient: {
      id: recipientId
    },
    message: {
      attachment: {
        type: "template",
        payload: {
          template_type: "generic",
          elements: [{
            title: "Dates",
            subtitle: paystring,
            buttons: [ {
              type: "postback",
              title: "Confirm Dates",
              payload: paystring,
            }]
          }]
        }
      }
    }
  };  

  callSendAPI(messageData);
}

function callSendAPI(messageData) {
  request({
    uri: 'https://graph.facebook.com/v2.6/me/messages',
    qs: { access_token: config["page_access_token"]},
    method: 'POST',
    json: messageData

  }, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      var recipientId = body.recipient_id;
      var messageId = body.message_id;
    } else {
      console.error("Unable to send message.");
      console.error(response);
      console.error(error);
    }
  });  
}
//========================================================================================//



